package com.polaris.mapper;

import com.polaris.entity.Blog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Polaris
 * @since 2021-07-25
 */
public interface BlogMapper extends BaseMapper<Blog> {

}
