package com.polaris.service.impl;

import com.polaris.entity.User;
import com.polaris.mapper.UserMapper;
import com.polaris.service.IUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Polaris
 * @since 2021-07-25
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements IUserService {

}
