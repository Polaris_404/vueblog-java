package com.polaris.service.impl;

import com.polaris.entity.Blog;
import com.polaris.mapper.BlogMapper;
import com.polaris.service.IBlogService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Polaris
 * @since 2021-07-25
 */
@Service
public class BlogServiceImpl extends ServiceImpl<BlogMapper, Blog> implements IBlogService {

}
