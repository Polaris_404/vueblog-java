package com.polaris.service;

import com.polaris.entity.Blog;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Polaris
 * @since 2021-07-25
 */
public interface IBlogService extends IService<Blog> {

}
