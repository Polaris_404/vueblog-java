package com.polaris.shiro;

import org.apache.shiro.authc.AuthenticationToken;

/**
 * @description:
 * @author: HouTianYun
 * @create: 2021-07-25 10:12
 **/
public class JwtToken implements AuthenticationToken {

    private String token;
    public JwtToken(String token) {
        this.token = token;
    }

    @Override
    public Object getPrincipal() {
        return token;
    }

    @Override
    public Object getCredentials() {
        return token;
    }
}
